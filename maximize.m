function [result, history] = maximize(fitnessFn, selectionFn, ...
    crossoverFn, mutationFn, stopFn, constraints, config)

    N = config.N;
    Gmax = config.maxGen;
    pm = config.pm;
    pc = config.pc;
    baseProblem = config.baseProblem;
    
    [Ncities, ~] = size(baseProblem);
        
    history = zeros(Gmax, Ncities + 1);
    g = 1;
        
    % Fitness results
    fitnesses = zeros(N, 1);
    
    % Initial population.
    pop = zeros(N, Ncities);
    
    for n = 1:N
        pop(n, :) = randperm(Ncities);
    end
    
    if config.lambda == 1 || config.lambda == 2
        [history, ~] = steadyState(fitnessFn, selectionFn, crossoverFn, ...
            mutationFn, stopFn, constraints, config, pop);
    else
        while (~stopFn(g, history, config.stopArgs))
            if (g == 1)
                % Fitness results
                for p = 1:N
                    fitnesses(p) = fitnessFn(baseProblem(pop(p, :), :));
                end
            end

            % Normalization of negative fitnesses values.
            fitnesses = normalize(fitnesses);

            % Mating pool selection
            selected = selectionFn(fitnesses, config.selectionArgs);

            % Assignment of parents pairs by permutation (1-2, 3-4, ...)
            matingPerm = randperm(length(selected));
            matingPool = selected(matingPerm);

            % Crossover
            children = crossoverFn(pop(matingPool, :), pc);

            % Mutation
            children = mutationFn(children, pm);

            % Record the best individual of the generation
            % and get new fitness results.
            [currentBest, fitnesses] = pickBest(children, baseProblem, fitnessFn);
            history(g, :) = currentBest;

            pop = children;
            g = g + 1;
        end
    end
    
    % Pick the best individual across every generation.
    result = pickBest(history(:, 1:end-1), baseProblem, fitnessFn);
end