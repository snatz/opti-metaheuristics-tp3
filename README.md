## M�taheuristiques en optimisation - TP 3

### Fait

* Structure de base de l'algorithme g�n�tique mono-objectif.
* La technique "codage du chemin".
* Le croisement bas� sur la position.
* Le croisement bas� sur le cycle.
* Le croisement partiel.
* La mutation d'�change.
* La mutation d'insertion.
* La mutation de d�placement.
* La mutation d'inversion.
* Les graphes.
* Titeuf.

Simon Bar & Thomas D�fossez