function norm = normalize(fitnesses)
    len = length(fitnesses);
    norm = zeros(len, 1);
    minFit = min(fitnesses);
    
    if (minFit < 0)
        norm(:) = fitnesses(:) + 2*abs(minFit);
    else
        norm = fitnesses;
    end
end