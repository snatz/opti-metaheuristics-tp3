function distance = tourDistance(cities)
% TOURDISTANCE  Compute the distance for the cities' tour.

    cities = [cities; cities(1, :)];  % Round tour!
    distances = sqrt(sum((cities(1:end-1, :) - cities(2:end, :)).^2, 2));
    distance = sum(distances);
end
