function [result, fitnesses] = pickBest(pop, baseProblem, fitnessFn)
    [N, ~] = size(pop);
    [Ncities, ~] = size(baseProblem);
    fitnesses = zeros(N, 1);
    result = zeros(Ncities + 1, 1);
    
    for p = 1:N
        fitnesses(p) = fitnessFn(baseProblem(pop(p, :), :));
    end
    
    [best, idx] = max(fitnesses(:, end));
        
    result(1:end-1) = pop(idx, :);
    result(end) = best;
end

