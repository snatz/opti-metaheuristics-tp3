function prob = generateProblem(N, constraints)
    % Generate a random problem
    [count, ~] = size(constraints);
    c = constraints';
    
    prob = c(1, :) + rand(N, count) .* (c(2, :) - c(1, :));
end