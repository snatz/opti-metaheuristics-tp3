function config = geneticConfig(N, maxGen, crossProb, mutProb, ...
    baseProblem, lambda, selectionArgs, stopArgs)
    % Configuration containing the population size, max number of 
    % generation, probabilities of crossover and mutation, 
    % and additional arguments.
    config.N = N;
    config.maxGen = maxGen;
    config.pc = crossProb;
    config.pm = mutProb;
    config.selectionArgs = selectionArgs;
    config.stopArgs = stopArgs;
    config.lambda = lambda;
    config.baseProblem = baseProblem;
end
