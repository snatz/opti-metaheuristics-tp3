function pop = randomIn(N, min, max)
    % Generate a random vector/matrix.
    [count, ~] = size(min);
    
    pop = min + rand(N, count) .* (max - min);
end