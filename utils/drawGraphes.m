function drawGraphes(baseProblem, history, result, maximizing)
    if (maximizing == 1)
        [~, bestIdx] = max(history(:, end));
    else
        [~, bestIdx] = min(history(:, end));
    end

    [N, ~] = size(baseProblem);

    % (Iterations, Fitness) + Best individual
    figure(1);
    clf;
    
    plot(linspace(1, length(history), length(history)), history(:, end));
    hold on;
    plot(bestIdx, result(end), '+r');
    hold off;
    
    % Salesman route
    figure(2);
    clf;
    
    for p = 1:N
        plot(baseProblem(p, 1), baseProblem(p, 2), '+b');
        hold on;
    end
    
    tourX = [baseProblem(result(1:end-1), 1); baseProblem(result(1), 1)];
    tourY = [baseProblem(result(1:end-1), 2); baseProblem(result(1), 2)];

    plot(tourX, tourY, '-b');
    hold off;
end
