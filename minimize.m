function [result, history] = minimize(fitnessFn, selectionFn, ...
    crossoverFn, mutationFn, stopFn, constraints, config)

    % Minimizing f is maximizing -f.
    [result, history] = maximize(@(varargin) -1*fitnessFn(varargin{:}), ...
        selectionFn, crossoverFn, mutationFn, stopFn, constraints, config);
    
    history(:, end) = history(:, end) .* -1;
    result(end) = result(end) * -1;
end
