function stopping = fitnessValueStop(g, history, args)
    Fmax = args(1);

    if (g <= 1 || history(g-1, 3) < Fmax)
        stopping = 0;
    else
        stopping = 1;
    end
end

