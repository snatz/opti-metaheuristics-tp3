function selected = stochasticUniversalSamplingSelection(fitnesses, args)
% STOCHASTICUNIVERSALSAMPLINGSELECTION Select the individuals based on the 
% stochastic universal sampling fitness scaling and the roulette wheel 
% selection algorithms.

    N = length(fitnesses);
    sumFit = sum(fitnesses);
    delta = 1/N;
    start = rand * delta;
    
    % Fitness-relative probabilities of individuals
    probabilities = zeros(N, 1);
    
    % Wheel pointers
    pointers = zeros(N, 1);
    
    % Probabilities computation
    for prob = 1:N
        probabilities(prob) = fitnesses(prob)/sumFit;
    end
    
    % Pointers computation
    for n = 1:N
        pointers(n) = start + (n-1) * delta;
    end
    
    % Selection
    selected = wheelSelection(probabilities, pointers);
end