function selected = linearScalingSelection(fitnesses, args)
% LINEARSCALINGSELECTION  Select the individuals based on the linear
% fitness scaling and the roulette wheel selection algorithms.

    N = length(fitnesses);
    av = mean(fitnesses);
    minFit = min(fitnesses);
    maxFit = max(fitnesses);

    fitnessesScaled = zeros(N, 1);
    
    for f = 1:N
        fitnessesScaled(f) = fitnesses(f) * (av / (av - minFit)) + ...
            (av * minFit) / (maxFit - minFit);
        
        if (fitnessesScaled(f) < 0)
            fitnessesScaled(f) = 0;
        end
    end

    selected = rouletteWheelSelection(fitnessesScaled, args);
end

