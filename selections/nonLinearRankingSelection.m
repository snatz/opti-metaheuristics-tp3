function selected = nonLinearRankingSelection(fitnesses, args)
% NONLINEARRANKINGSELECTION  Select the individuals based on a ranking 
% algorithm (non-linear). The argument needed is alpha (in [0,1]).

    N = length(fitnesses);
    alpha = args(1);
    fitSorted = sort(fitnesses, 'descend');
    sortedIndexes = zeros(N, 1);
    
    if (alpha <= 0 || alpha >= 1)
        alpha = rand;
    end
    
    for i = 1:N
        sortedIndexes(i) = find(fitSorted == fitnesses(i), 1) - 1;
    end
    
    % Fitness-relative probabilities of individuals
    probabilities = zeros(N, 1);
    
    % Probabilities computation
    for i = 1:N
        probabilities(i) = alpha * ((1 - alpha)^(N - sortedIndexes(i)));
    end
    
    % Selection
    selected = wheelSelection(probabilities, rand(N, 1));
end
