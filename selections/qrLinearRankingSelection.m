function selected = qrLinearRankingSelection(fitnesses, args)
% QRLINEARRANKINGSELECTION  Select the individuals based on a ranking 
% algorithm (q-r). The arguments needed are q and r (q == r*(N-1)/2 + 1/N),
% with N the population length.

    N = length(fitnesses);
    q = args(1);
    r = args(2);
    fitSorted = sort(fitnesses, 'descend');
    sortedIndexes = zeros(N, 1);
    
    if (q ~= r*(N-1)/2 + 1/N)
        % Some default values.
        r = 2/(N*(N-1));
        q = 2/N;
    end
    
    for i = 1:N
        sortedIndexes(i) = find(fitSorted == fitnesses(i), 1);
    end
    
    % Fitness-relative probabilities of individuals
    probabilities = zeros(N, 1);
    
    % Probabilities computation
    for i = 1:N
        probabilities(i) = q - (sortedIndexes(i) - 1) * r;
    end
    
    % Selection
    selected = wheelSelection(probabilities, rand(N, 1));
end
