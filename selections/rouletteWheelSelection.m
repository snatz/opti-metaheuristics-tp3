function selected = rouletteWheelSelection(fitnesses, args)
% ROULETTEWHEELSELECTION  Select the individuals based on the roulette
% wheel selection, with N random pointers.

    N = length(fitnesses);
    sumFit = sum(fitnesses);
    
    % Fitness-relative probabilities of individuals
    probabilities = zeros(N, 1);
    
    % Probabilities computation
    for prob = 1:N
        probabilities(prob) = fitnesses(prob)/sumFit;
    end

    % Selection
    selected = wheelSelection(probabilities, rand(N, 1));
end
