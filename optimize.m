function [result, history] = optimize(fitnessFn, selectionFn, crossoverFn, ...
    mutationFn, stopFn, constraints, config, maximizing)

    tic
    if (maximizing == 1)
        [result, history] = maximize(fitnessFn, selectionFn, crossoverFn, ...
            mutationFn, stopFn, constraints, config);
    else
        [result, history] = minimize(fitnessFn, selectionFn, crossoverFn, ...
            mutationFn, stopFn, constraints, config);
    end
    toc
    
    % Draw graphes
    drawGraphes(config.baseProblem, history, result, maximizing);
end