% Add subpaths to Matlab global path. Allows global usage of GA functions.
% Only need to be executed once.
addpath(genpath('.'));

selectionFn = @tournamentSelection;
crossoverFn = @partialCrossover;
mutationFn = @moveMutation;
stopFn = @maxGenerationStop;

Ncities = 12;
constraints = [0 10; 0 10];
N = 200;
Gmax = 500;
pc = 0.9;
pm = 0.25;
lambda = 0;
maximizing = 0;

% Initialize the problem
problem = generateProblem(Ncities, constraints);
    
% Population length, max generation, chrom. length, crossover prob.
% mutation prob., lambda, additional arguments for selection and stop
% criteria.
config = geneticConfig(N, Gmax, pc, pm, problem, lambda, ...
    [2], [Gmax]);

[result, history] = optimize(@tourDistance, selectionFn, crossoverFn, ...
    mutationFn, stopFn, constraints, config, maximizing);

disp(result);
