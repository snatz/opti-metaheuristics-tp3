function pop = insertMutation(pop, pm)
% INSERTMUTATION  Apply the insertion mutation to a permutation.

    [N, Ncities] = size(pop);
    
    for n = 1:N
        if (rand < pm)
            % Choose a random locus and insertion index .
            locusIndex = randi(Ncities);
            insertIndex = randi(Ncities);
            
            loc = pop(n, locusIndex);
            
            % Mark the old locus index.
            pop(n, locusIndex) = 0;
            ind = [pop(n, 1:insertIndex) loc pop(n, (insertIndex+1):end)];

            % Remove the old interval.
            [~, zer] = find(ind == 0);
            ind(zer) = [];
            pop(n, :) = ind;
        end
    end
end
