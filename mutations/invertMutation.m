function pop = invertMutation(pop, pm)
% INVERTMUTATION  Apply the inversion mutation to a permutation.

    [N, Ncities] = size(pop);
    
    for n = 1:N
        if (rand < pm)
            % Choose a random locus and insertion index and a random length.
            locusIndex = randi(Ncities);
            insertIndex = randi(Ncities);
            
            if (Ncities - locusIndex ~= 0)
                invertLength = randi(Ncities - locusIndex);
            else
                continue;
            end
            
            % Flip the chosen interval.
            inverted = fliplr(pop(n, locusIndex:(locusIndex + invertLength)));
            
            % Mark the old interval.
            pop(n, locusIndex:(locusIndex + invertLength)) = 0;
            ind = [pop(n, 1:insertIndex) inverted pop(n, (insertIndex+1):end)];

            % Remove the old interval.
            [~, zer] = find(ind == 0);
            ind(zer) = [];
            pop(n, :) = ind;
        end
    end
end
