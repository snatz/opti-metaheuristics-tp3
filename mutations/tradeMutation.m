function pop = tradeMutation(pop, pm)
% TRADEMUTATION  Apply the trade mutation to a permutation.

    [N, Ncities] = size(pop);
    
    for n = 1:N
        if (rand < pm)
            % Choose 2 random indexes per permutation.
            tradeIndexes = randi(Ncities, 2, 1);

            % Swap both elements.
            temp = pop(n, tradeIndexes(1));
            pop(n, tradeIndexes(1)) = pop(n, tradeIndexes(2));
            pop(n, tradeIndexes(2)) = temp;
        end
    end
end
