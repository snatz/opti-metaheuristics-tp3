function pop = cycleCrossover(pop, pc)
 % CYCLECROSSOVER  Apply the cycle crossover to the permutation.

    [N, Ncities] = size(pop);
    
    for n = 1:N/2
        if (rand < pc)
            parent1 = pop(2*n - 1, :);
            parent2 = pop(2*n, :);
            
            % First child
            child1 = zeros(1, Ncities);
            start = 1;
            count = 0;

            while (count < Ncities)
                cycle = findVirtualCycle(start, parent1, parent2);
                count = count + length(cycle(1:end-1));
                child1 = copyCycle(cycle, parent1, child1);
                
                if (count < Ncities)
                    start = find(child1 == 0, 1, 'first');
                    cycle = findVirtualCycle(start, parent2, parent1);
                    count = count + length(cycle(1:end-1));
                    child1 = copyCycle(cycle, parent2, child1);
                end
                
                start = find(child1 == 0, 1, 'first');
            end
            
            % Second child.
            child2 = zeros(1, Ncities);
            start = 1;
            count = 0;

            while (count < Ncities)
                cycle = findVirtualCycle(start, parent2, parent1);
                count = count + length(cycle(1:end-1));
                child2 = copyCycle(cycle, parent2, child2);
                
                if (count < Ncities)
                    start = find(child2 == 0, 1, 'first');
                    cycle = findVirtualCycle(start, parent1, parent2);
                    count = count + length(cycle(1:end-1));
                    child2 = copyCycle(cycle, parent1, child2);
                end
                
                start = find(child2 == 0, 1, 'first');
            end
                        
            pop(2*n - 1, :) = child1;
            pop(2*n, :) = child2;
        end
    end
end

function cycle = findVirtualCycle(start, parent1, parent2)
    current = parent1(start);
    first = current;
    cycle = [current];
    
    while (current ~= first || length(cycle) == 1)
        otherIndex = parent2 == current;
        current = parent1(otherIndex);
        cycle = [cycle current];
    end
end

function child = copyCycle(cycle, parent, child)
    for c = 1:length(cycle)
        idx = parent == cycle(c);
        child(idx) = parent(idx);
    end
end
