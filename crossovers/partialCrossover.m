function pop = partialCrossover(pop, pc)
% PARTIALCROSSOVER  Apply the partial crossover to a permutation.

    [N, Ncities] = size(pop);
    
    for n = 1:N/2
        if (rand < pc)
            parent1 = pop(2*n - 1, :);
            parent2 = pop(2*n, :);
            
            %Getting substring to permute
            substringStart = randi(Ncities);
            substringEnd = randi(Ncities);
            if substringStart > substringEnd
                temp = substringStart;
                substringStart = substringEnd;
                substringEnd = temp;
            end

            %Permute substrings
            tempSubstring = parent1(substringStart:substringEnd);
            parent1(substringStart:substringEnd) = parent2(substringStart:substringEnd);
            parent2(substringStart:substringEnd) = tempSubstring;
            
            %Getting mapping relationships for restitutions
            substringSize = substringEnd - substringStart + 1;
            mappingRelationships = zeros(substringSize, 2);
            mappingRelationships(:, 1) = parent1(substringStart:substringEnd);
            mappingRelationships(:, 2) = parent2(substringStart:substringEnd);
            j = 1;
            for i = 1:size(mappingRelationships, 1)
                if j <= size(mappingRelationships, 1)
                    if mappingRelationships(j, 1) == mappingRelationships(j, 2)
                        mappingRelationships(j, :) = [];
                        j = j - 1;
                    end
                end
                j = j + 1;
            end
            
            %Fixing children
            for i = 1:size(mappingRelationships, 1)
                indexesParent1 = find(parent1 == mappingRelationships(i, 1));
                indexesParent2 = find(parent2 == mappingRelationships(i, 2));
                if(size(indexesParent1, 1) > 1)
                    if(indexesParent1(1, 1) < substringStart)
                        indexParent1 = indexesParent1(1, 1);
                    else
                        if(indexesParent1(1, 2) > substringEnd)
                            indexParent1 = indexesParent1(1, 1);
                        else
                            indexParent1 = indexesParent1(2, 1);
                        end
                    end
                else
                    indexParent1 = indexesParent1(1, 1);
                end
                if(size(indexesParent2, 1) > 1)
                    if(indexesParent2(1, 1) < substringStart)
                        indexParent2 = indexesParent2(1, 1);
                    else 
                        if(indexesParent2(1, 2) > substringEnd)
                            indexParent2 = indexesParent2(1, 1);
                        else
                            indexParent2 = indexesParent2(2, 1);
                        end
                    end
                else
                    indexParent2 = indexesParent2(1, 1);
                end
                temp = parent1(indexParent1);
                parent1(indexParent1) = parent2(indexParent2);
                parent2(indexParent2) = temp;
            end

            % Register children.
            pop(2*n - 1, :) = parent1;
            pop(2*n, :) = parent2;
        end
    end
end
