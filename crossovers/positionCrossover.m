function pop = positionCrossover(pop, pc)
% POSITIONCROSSOVER  Apply the position crossover to a permutation.

    [N, Ncities] = size(pop);
    
    for n = 1:N/2
        if (rand < pc)
            parent1 = pop(2*n - 1, :);
            parent2 = pop(2*n, :);
            
            Nlocus = randi(Ncities);
            
            % Choose n random indexes.
            posIndexes = randperm(Ncities, Nlocus);

            % Copy chosen locuses.
            children1 = zeros(Ncities, 1);
            children1(posIndexes) = parent1(posIndexes);
            
            % Copy leftover locuses from parent2 to parent1.
            locusesLeft = setdiff(parent2, children1, 'stable');
            children1(children1 == 0) = locusesLeft;
            
            % Invert roles.
            posIndexes = randi(Ncities, Nlocus, 1);

            children2 = zeros(Ncities, 1);
            children2(posIndexes) = parent2(posIndexes);
            
            locusesLeft = setdiff(parent1, children2, 'stable');
            children2(children2 == 0) = locusesLeft;
            
            % Register children.
            pop(2*n - 1, :) = children1;
            pop(2*n, :) = children2;
        end
    end
end
